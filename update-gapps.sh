#!/usr/bin/env sh
#
# Copyright (C) 2019 shagbag913
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if ! echo "$PWD" | grep -q 'gapps$'; then
    echo "This script must be ran in the gapps directory! Aborting..."
    exit 1
fi

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "System or product directory not specified"
    echo "Usage:"
    echo "$0 <SYSTEM IMAGE PATH> <PRODUCT IMAGE PATH>"
    exit 1
fi

SYSTEM="$1"
PRODUCT="$2"

# List of updatable files
FILE_UPDATE_LIST="$(find . \( -path ./.git -o -name "*.mk" -o -name "*.sh" -o -path ./overlay -o \
    -name "*default-permissions.xml" -o -path ./PixelSetupWizardOverlay \) -prune -o -type f -print | sed 's/\.\///')"

copy_gapp() {
    if cp "$1" "$2"; then
        echo "[+] Updated $2"
    else
        echo "[-] Failed to update $2"
    fi
}

# Update updatable files
for file in $FILE_UPDATE_LIST; do
    # MatchmakerPrebuilt has a different module name on stock for the different Pixels,
    # although upon decompiling them and diffing them they seem the exact same.
    if [ "$file" = "priv-app/MatchmakerPrebuilt/MatchmakerPrebuilt.apk" ]; then
        copy_gapp "$(find "$PRODUCT/priv-app" -name "MatchmakerPrebuilt*.apk")" "$file"
        continue
    fi

    if [ -f "$PRODUCT/$file" ]; then
        PARTITION="$PRODUCT"
    elif [ -f "$SYSTEM/$file" ]; then
        PARTITION="$SYSTEM"
    else
        echo "[-] $file not found in factory image, skipping"
        continue
    fi

    copy_gapp "$PARTITION/$file" "$file"

    # Correct com.qualcomm.qcrilmsgtunnel package name typo
    if [ "$file" = "etc/sysconfig/nexus.xml" ]; then
        if sed -i 's/com\.qulacomm\.qcrilmsgtunnel/com\.qualcomm\.qcrilmsgtunnel/' "$file"; then
            echo "[+] Corrected qcrilmsgtunnel package name typo in $file"
        else
            echo "[-] Failed to correct qcrilmsgtunnel package name typo in $file"
        fi
    fi
done
