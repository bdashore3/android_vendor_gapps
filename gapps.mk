# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# App
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleCamera \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    MarkupGoogle \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    Photos

# Pixel 4 Google Assistant resources
ifneq ($(filter coral flame, $(lastword $(subst _, ,$(TARGET_PRODUCT)))),)
PRODUCT_PACKAGES += \
    NgaResources
endif

PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.dialer.support

# Priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    CarrierServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleDialer \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    NexusLauncherRelease \
    OTAConfigPrebuilt \
    PixelSetupWizard \
    PrebuiltGmsCoreQt \
    PrebuiltGmsCoreQt_AdsDynamite \
    PrebuiltGmsCoreQt_CronetDynamite \
    PrebuiltGmsCoreQt_DynamiteLoader \
    PrebuiltGmsCoreQt_DynamiteModulesA \
    PrebuiltGmsCoreQt_DynamiteModulesC \
    PrebuiltGmsCoreQt_GoogleCertificates \
    PrebuiltGmsCoreQt_MapsDynamite \
    PrebuiltGmsCoreQt_MeasurementDynamite \
    Phonesky \
    SetupWizardPrebuilt \
    Velvet

# RRO Overlays
PRODUCT_PACKAGES += \
    PixelSetupWizardOverlay

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/

# Enable Google Assistant
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

$(call inherit-product, vendor/gapps/gapps-blobs.mk)
